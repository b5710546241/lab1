package oop.lab1;

/**
 * A simple model for a student that extends from Person class with an id. 
 * @author Tuangrat Mungmeerattanaworachot 5710546241
 *
 */
public class Student extends Person {
	private long id;
	
	/**
	 * Initialize a new student.
	 * @param name is the name of student.
	 * @param id is the id of student.
	 */
	public Student(String name, long id) {
		super(name); // name is managed by Person
		this.id = id;
	}

	/** return a string representation of this Student. */
	public String toString() {
		return String.format("Student %s (%d)", getName(), id);
	}

	/**
	 * Compare student's by name and id.
	 * They are equal if the name and the id match.
	 * @param other is another Student to compare to this one.
	 * @return true if the name and the id are the same, false otherwise.
	 */
	public boolean equals(Object obj) {
		
		// (1) verify that obj is not null
			if(obj==null) 
				return false;
				
		// (2) test if obj is the same class as "this" object
			if(obj.getClass() != this.getClass()) 
				return false;
				
		// (3) cast obj to this class's type
			Student other = (Student) obj;
		
		// (4) compare whatever values determine "equals"
		if(this.getName().equalsIgnoreCase(other.getName()) &&
				this.id == other.id) 
			return true;
		
		return false;
	}
}
