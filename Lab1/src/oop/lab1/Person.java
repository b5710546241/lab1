package oop.lab1;
/**
 * A simple model for a person with a name.
 * 
 * @author Tuangrat Mungmeerattanaworachot 5710546241
 */
public class Person {
	/** the person's name. may contain spaces. */
	private String name;
	
	/**
	 * Initialize a new Person object.
	 * @param name is the name of the new Person
	 */
	public Person(String name) {
		this.name = name;
	}
	
	/**
	 * Get the person's name.
	 * @return the name of this person
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Compare person's by name.
	 * They are equal if the name matches.
	 * @param other is another Person to compare to this one.
	 * @return true if the name is same, false otherwise.
	 */
	public boolean equals(Object obj) {
		
		// (1) verify that obj is not null
		if(obj==null) 
			return false;
		
		// (2) test if obj is the same class as "this" object
		if(obj.getClass() != this.getClass()) 
			return false;
		
		// (3) cast obj to this class's type
		Person other = (Person) obj;
		
		// (4) compare whatever values determine "equals"
		if(this.name.equalsIgnoreCase(other.getName())) 
			return true;
		
		return false;
	}
	
	/**
	 * Get a string representation of this Person.
	 */
	public String toString() {
		return "Person " + name;
	}
}
